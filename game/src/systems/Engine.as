package systems
{
	import com.ktm.genome.core.data.gene.GeneManager;
	import com.ktm.genome.core.entity.family.Family;
	import com.ktm.genome.core.entity.family.matcher.allOfGenes;
	import com.ktm.genome.core.entity.family.matcher.noneOfGenes;
	import com.ktm.genome.core.entity.IEntity;
	import com.ktm.genome.core.entity.IEntityManager;
	import com.ktm.genome.core.logic.system.System;
	import com.ktm.genome.game.system.GameLevelSystem;
	import com.ktm.genome.render.component.Transform;
	import com.ktm.genome.resource.component.TextureResource;
	import components.Atom;
	import components.Bullet;
	import components.Cannon;
	import components.Entry;
	import components.ImageTexte;
	import components.Meteor;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	import MoPPliq.manager.MoPPliqInterpretorManager;
	import unitcircle.EntityFactory;
	import unitcircle.EntriesTable;
	import unitcircle.KeyboardTable;
	
	public class Engine extends System
	{
		[Inject]
		public var gameLevelSystem:GameLevelSystem;
		[Inject]
		public var moppliq:MoPPliqInterpretorManager;
		
		private var m_em:IEntityManager;
		private var m_gene:GeneManager;
		private var m_keyboard:KeyboardTable;
		private var m_side:int;
		private var m_elapsed:Number;
		private var m_missed:int;
		private var m_entriesTable:EntriesTable;
		private var m_started:Boolean;
		private var m_moppliqHash:Dictionary;
		private var m_moppliqLaunched:Boolean;
		
		public function Engine(em:IEntityManager, gene:GeneManager, keyboard:KeyboardTable, side:int)
		{
			super();
			
			m_em = em;
			m_gene = gene;
			m_keyboard = keyboard;
			m_side = side;
			m_elapsed = 0.0;
			m_missed = 0;
			m_entriesTable = new EntriesTable(m_em, m_gene, m_keyboard);
			m_started = false;
			m_moppliqLaunched = false;
		}
		
		override protected function onConstructed():void
		{
			super.onConstructed();
		}
		
		override protected function onProcess(delta:Number):void
		{
			if (!m_moppliqLaunched && moppliq.isReady())
			{
				gameLevelSystem.startLevel(moppliq.startLinkSet());
				m_moppliqLaunched = true;
			}
			
			if (m_started)
			{
				super.onProcess(delta);
				
				m_elapsed += delta / 1000;
				
				this.updateCanon(delta);
				this.updateBullets(delta);
				this.updateMeteors(delta);
				m_entriesTable.update(delta);
				this.draw();
			}
		}
		
		protected function updateCanon(delta:Number):void
		{
			var entity:Family = m_em.getFamily(allOfGenes(Atom, Cannon));
			
			if (entity.member)
			{
				var atom:Atom = m_gene.getComponentMapper(Atom).getComponent(entity.member);
				var cannon:Cannon = m_gene.getComponentMapper(Cannon).getComponent(entity.member);
				
				if (m_keyboard.isKeyDown(Keyboard.LEFT))
				{
					cannon.rad += delta / 1000 * cannon.speed;
				}
				
				if (m_keyboard.isKeyDown(Keyboard.RIGHT))
				{
					cannon.rad -= delta / 1000 * cannon.speed;
				}
				
				cannon.rad = cannon.rad % (2 * Math.PI);
				cannon.shotTime += delta / 1000;
				atom.rotation = cannon.rad;
				atom.visible = true;
				
				if (m_keyboard.isKeyDown(Keyboard.SPACE) && cannon.fireSpeed < cannon.shotTime)
				{
					EntityFactory.createBullet(m_em, cannon.rad, atom.radius * 3, 1.0, 1.0);
					cannon.shotTime = 0.0;
					atom.radius = 0.045;
				}
				else
				{
					atom.radius = 0.050;
				}
			}
		}
		
		protected function updateBullets(delta:Number):void
		{
			var entities:Family = m_em.getFamily(allOfGenes(Atom, Bullet));
			
			for (var i:int = 0; i < entities.members.length; i++)
			{
				var e:IEntity = entities.members[i];
				var atom:Atom = m_gene.getComponentMapper(Atom).getComponent(e);
				var bullet:Bullet = m_gene.getComponentMapper(Bullet).getComponent(e);
				
				if (atom.dist > 1.0)
				{
					m_em.killEntity(e);
				}
				else
				{
					atom.dist += delta / 1000 * bullet.speed;
					atom.visible = true;
				}
			}
			
			this.checkImpacts();
		}
		
		protected function updateMeteors(delta:Number):void
		{
			var entities:Family = m_em.getFamily(allOfGenes(Atom, Meteor));
			
			if (entities.members.length == 0 && m_entriesTable.endtime() < m_elapsed && m_entriesTable.endtime() > 0)
			{
				m_started = false;
				gameLevelSystem.endLevel();
			}
			
			for (var i:int = 0; i < entities.members.length; i++)
			{
				var e:IEntity = entities.members[i];
				var atom:Atom = m_gene.getComponentMapper(Atom).getComponent(e);
				var meteor:Meteor = m_gene.getComponentMapper(Meteor).getComponent(e);
				
				if (atom.dist < 0.0)
				{
					m_em.killEntity(e);
					m_missed += 1;
				}
				else
				{
					if (meteor.life > 0)
					{
						atom.dist -= delta / 1000 * meteor.speed;
						atom.rotation = (atom.rotation + (Math.PI * delta / 1000)) % (Math.PI * 2);
					}
					else
					{
						if (atom.radius > 0.001)
						{
							atom.radius -= 0.005 * m_elapsed;
						}
						else
						{
							m_em.killEntity(e);
						}
					}
					
					atom.visible = true;
				}
			}
			
			this.checkImpacts();
		}
		
		protected function checkImpacts():void
		{
			var bullets:Family = m_em.getFamily(allOfGenes(Atom, Bullet));
			
			for (var i:int = 0; i < bullets.members.length; i++)
			{
				var eBullet:IEntity = bullets.members[i];
				var aBullet:Atom = m_gene.getComponentMapper(Atom).getComponent(eBullet);
				var bullet:Bullet = m_gene.getComponentMapper(Bullet).getComponent(eBullet);
				var meteors:Family = m_em.getFamily(allOfGenes(Atom, Meteor));
				
				for (var j:int = 0; j < meteors.members.length; j++)
				{
					var eMeteor:IEntity = meteors.members[j];
					var aMeteor:Atom = m_gene.getComponentMapper(Atom).getComponent(eMeteor);
					var meteor:Meteor = m_gene.getComponentMapper(Meteor).getComponent(eMeteor);
					
					var abx:Number = aBullet.dist * Math.cos(aBullet.rad);
					var aby:Number = aBullet.dist * Math.sin(aBullet.rad);
					var amx:Number = aMeteor.dist * Math.cos(aMeteor.rad);
					var amy:Number = aMeteor.dist * Math.sin(aMeteor.rad);
					var d:Number = Math.sqrt(Math.pow(amx - abx, 2) + Math.pow(amy - aby, 2));
					
					if (d < (aBullet.radius + aMeteor.radius) && meteor.life > 0)
					{
						meteor.life -= bullet.dmg;
						m_em.killEntity(eBullet);
					}
				}
			}
		}
		
		protected function draw():void
		{
			var entities:Family = m_em.getFamily(allOfGenes(Atom, Transform, TextureResource));
			var levelText:Family = m_em.getFamily(allOfGenes(ImageTexte, Transform), noneOfGenes(Entry));
			
			if (levelText.members.length == 1)
			{
				var levelEntity:IEntity = levelText.member;
				var text:ImageTexte = m_gene.getComponentMapper(ImageTexte).getComponent(levelEntity);
				
				text.text = moppliq.getCurrentActivityName() + " (missed " + m_missed + ")";
			}
			
			for (var i:int = 0; i < entities.members.length; i++)
			{
				var e:IEntity = entities.members[i];
				var atom:Atom = m_gene.getComponentMapper(Atom).getComponent(e);
				var transform:Transform = m_gene.getComponentMapper(Transform).getComponent(e);
				var texture:TextureResource = m_gene.getComponentMapper(TextureResource).getComponent(e);
				
				var min:Number = Math.min(texture.bitmapData.width, texture.bitmapData.height);
				transform.scaleX = atom.radius / (min / m_side);
				transform.scaleY = atom.radius / (min / m_side);
				var texW:Number = texture.bitmapData.width * transform.scaleX;
				var texH:Number = texture.bitmapData.height * transform.scaleY;
				var tx:Number = texW / 2 * Math.cos(-atom.rotation) - (texH / 2) * Math.sin(-atom.rotation);
				var ty:Number = texW / 2 * Math.sin(-atom.rotation) + (texH / 2) * Math.cos(-atom.rotation);
				var ax:Number = m_side / 2 * atom.dist * Math.cos(atom.rad);
				var ay:Number = m_side / 2 * atom.dist * Math.sin(atom.rad);
				
				transform.x = m_side / 2 - tx + ax;
				transform.y = m_side / 2 - ty - ay;
				transform.rotation = -atom.rotation / Math.PI * 180.0;
				transform.visible = atom.visible;
			}
		}
		
		public function init():void
		{
			m_elapsed = 0.0;
			m_missed = 0;
			m_entriesTable = new EntriesTable(m_em, m_gene, m_keyboard);
			m_started = true;
		}
		
		public function clear():void
		{
			var textures:Family = m_em.getFamily(allOfGenes(Atom, Transform, TextureResource));
			
			var goalPerfect:String = m_moppliqHash[moppliq.getCurrentActivityName() + "_perfect"];
			var goalMissed:String = m_moppliqHash[moppliq.getCurrentActivityName() + "_missed"];
			 
			 for (var k:int = 0; k < textures.members.length; k++)
			{
				var e:IEntity = textures.members[k];
				var texture:TextureResource = m_gene.getComponentMapper(TextureResource).getComponent(e);
				
				if (texture.id != "cannon")
				{
					m_em.killEntity(e);
				}
			}
			
			if (m_missed > 0)
			{
				moppliq.addGoal(goalMissed);
			}
			else
			{
				moppliq.addGoal(goalPerfect);
			}
			
			gameLevelSystem.startLevel(moppliq.next());
		}
		
		public function loadMoppliq(moppliqString:String):void
		{
			var moppliqLoader:URLLoader = new URLLoader();
			
			moppliqLoader.addEventListener(Event.COMPLETE, completeHandler);
			
			moppliqLoader.load(new URLRequest(moppliqString));
		}
		
		private function completeHandler(event:Event):void
		{
			var level:XML = new XML(event.target.data);
			m_moppliqHash = new Dictionary();
			
			for each (var node:XML in level.game.goals.goal)
			{
				m_moppliqHash[String(node.name)] = node.@id_goal;
			}
		}
	}
}