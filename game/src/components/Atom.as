package components
{
	import com.ktm.genome.core.data.component.Component;

	public class Atom extends Component
	{
		public var rad:Number = 0.0;
		public var dist:Number = 1.0;
		public var radius:Number = 0.0;
		public var rotation:Number = 0.0;
		public var visible:Boolean = false;
	}
}