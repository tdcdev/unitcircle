package components
{
	import com.ktm.genome.core.data.component.Component;

	public class Cannon extends Component
	{
		public var rad:Number = 0.0;
		public var speed:Number = Math.PI;
		public var fireSpeed:Number = 0.3;
		public var shotTime:Number = 0;
	}
}