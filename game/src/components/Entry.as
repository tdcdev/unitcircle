package components
{
	import com.ktm.genome.core.data.component.Component;

	public class Entry extends Component
	{
		public var time:Number;
		public var remaind:Number;
		public var display:String;
		public var rad:Number;
		public var radius:Number;
		public var speed:Number;
		public var life:Number;
	}
}