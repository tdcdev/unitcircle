package components
{
	import com.ktm.genome.core.data.component.Component;

	public class Bullet extends Component
	{
		public var speed:Number = 1.0;
		public var dmg:Number = 1.0;
	}
}