package unitcircle
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;

	public class KeyboardTable extends Sprite
	{
		private var m_hash:Object = {};

		public function KeyboardTable()
		{
			this.addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
		}

		private function addedToStageHandler(e:Event):void
		{
			this.stage.addEventListener(KeyboardEvent.KEY_UP, keyUpHandler);
			this.stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
		}

		private function keyUpHandler(event:KeyboardEvent):void
		{
			delete m_hash[event.keyCode];
		}

		private function keyDownHandler(event:KeyboardEvent):void
		{
			m_hash[event.keyCode] = 1;
		}

		public function isKeyDown(code:uint):Boolean
		{
			return m_hash[code] !== undefined;
		}
	}
}