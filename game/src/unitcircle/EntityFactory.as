package unitcircle
{
	import com.ktm.genome.core.entity.IEntity;
	import com.ktm.genome.core.entity.IEntityManager;
	import com.ktm.genome.render.component.Layered;
	import com.ktm.genome.render.component.Transform;
	import com.ktm.genome.resource.component.EntityBundle;
	import com.ktm.genome.resource.component.TextureResource;
	import components.Atom;
	import components.Bullet;
	import components.Meteor;

	public class EntityFactory
	{
		static public function createResourcedEntity(em:IEntityManager, source:String, id:String):IEntity
		{
			var e:IEntity = em.create();
			em.addComponent(e, EntityBundle, {source: source, id: id, toBuild: true});
			return e;
		}

		static public function createBullet(em:IEntityManager, rad:Number, dist:Number, speed:Number, dmg:Number):IEntity
		{
			var e:IEntity = em.create();
			em.addComponent(e, Layered, {layerId: "bulletLayer"});
			em.addComponent(e, TextureResource, {id: "bullet"});
			em.addComponent(e, Transform, {visible: false});
			em.addComponent(e, Atom, {rad: rad, dist: dist, radius: 0.01, rotation: rad});
			em.addComponent(e, Bullet, {speed: speed, dmg: dmg});
			return e;
		}

		static public function createMeteor(em:IEntityManager, rad:Number, radius:Number, speed:Number, life:Number):IEntity
		{
			var e:IEntity = em.create();
			em.addComponent(e, Layered, {layerId: "meteorLayer"});
			em.addComponent(e, TextureResource, {id: "meteor"});
			em.addComponent(e, Transform, {visible: false});
			em.addComponent(e, Atom, {rad: rad, dist: 1.1, radius: radius});
			em.addComponent(e, Meteor, {speed: speed, life: life});
			return e;
		}
	}
}
