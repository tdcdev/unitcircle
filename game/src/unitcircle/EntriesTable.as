package unitcircle
{
	import com.ktm.genome.core.data.gene.GeneManager;
	import com.ktm.genome.core.entity.family.Family;
	import com.ktm.genome.core.entity.family.matcher.allOfGenes;
	import com.ktm.genome.core.entity.IEntity;
	import com.ktm.genome.core.entity.IEntityManager;
	import com.ktm.genome.render.component.Transform;
	import components.Entry;
	import components.ImageTexte;
	import unitcircle.EntityFactory;
	import unitcircle.EntriesTable;
	import unitcircle.KeyboardTable;
	
	public class EntriesTable
	{
		private var m_em:IEntityManager;
		private var m_gene:GeneManager;
		private var m_keyboard:KeyboardTable;
		private var m_elapsed:Number;
		private var m_endtime:int;
		
		public function EntriesTable(em:IEntityManager, gene:GeneManager, keyboard:KeyboardTable)
		{
			m_em = em;
			m_gene = gene;
			m_keyboard = keyboard;
			m_elapsed = 0.0;
			m_endtime = -1;
		}
		
		public function update(delta:Number):void
		{
			m_elapsed += delta / 1000;
			
			var entities:Family = m_em.getFamily(allOfGenes(Entry, ImageTexte, Transform));
			var tab:Array = new Array();
			
			for (var i:int = 0; i < entities.members.length; i++)
			{
				var e:IEntity = entities.members[i];
				var entry:Entry = m_gene.getComponentMapper(Entry).getComponent(e);
				var text:ImageTexte = m_gene.getComponentMapper(ImageTexte).getComponent(e);
				var transform:Transform = m_gene.getComponentMapper(Transform).getComponent(e);
				
				if (m_endtime < entry.time + entry.remaind)
				{
					m_endtime = entry.time + entry.remaind;
				}
				
				if (entry.time < m_elapsed)
				{
					var acc:int = int(entry.time + entry.remaind - m_elapsed);
					text.text = entry.display + "   (life " + entry.life + ") in " + String(acc) + "s";
					transform.y = acc;
					tab.push(transform);
				}
				
				if (entry.time + entry.remaind < m_elapsed)
				{
					m_em.killEntity(e);
					EntityFactory.createMeteor(m_em, entry.rad, entry.radius, entry.speed, entry.life);
				}
			}
			
			tab.sortOn("y", Array.NUMERIC);
			
			for (i = 0; i < tab.length; i++)
				tab[i].y = i * 15 + 20;
		}
		
		public function endtime():int
		{
			return m_endtime + 2;
		}
	}
}