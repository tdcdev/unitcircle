package unitcircle
{
	import com.ktm.genome.core.BigBang;
	import com.ktm.genome.core.IWorld;
	import com.ktm.genome.core.logic.process.ProcessPhase;
	import com.ktm.genome.core.logic.system.ISystemManager;
	import com.ktm.genome.core.logic.process.ProcessManager;
	import com.ktm.genome.core.entity.IEntityManager;
	import com.ktm.genome.core.data.gene.GeneManager;
	import com.ktm.genome.game.component.GameAction;
	import com.ktm.genome.game.component.Task;
	import com.ktm.genome.game.expression.DefaultOperatorManager;
	import com.ktm.genome.game.expression.IOperatorManager;
	import com.ktm.genome.game.manager.GameActionManager;
	import com.ktm.genome.game.manager.NodeManager;
	import com.ktm.genome.game.system.GameLevelSystem;
	import com.ktm.genome.game.system.GameTaskSystem;
	import com.ktm.genome.render.system.RenderSystem;
	import com.ktm.genome.resource.entity.ExtendedXMLEntityBuilder;
	import com.ktm.genome.resource.entity.IEntityBuilder;
	import com.ktm.genome.resource.manager.ResourceManager;
	import flash.display.Sprite;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import managers.ImageTexteManager;
	import systems.Engine;
	import systems.ImageTexteSystem;
	import MoPPliq.manager.MoPPliqInterpretorManager;
	
	[SWF(width="480",height="480",frameRate="30",backgroundColor="#FFFFFF")]
	
	public class Main extends Sprite
	{
		
		GameAction;
		Task;
		
		public function Main():void
		{
			var world:IWorld = BigBang.createWorld(stage);
			var sm:ISystemManager = world.getSystemManager();
			var keyboard:KeyboardTable = new KeyboardTable();
			var engine:Engine = new Engine(world.getEntityManager(), world.getGeneManager(), keyboard, stage.stageHeight);			
			var moppliq:MoPPliqInterpretorManager = new MoPPliqInterpretorManager();
			
			this.addChild(keyboard);
			
			world.setLogic(NodeManager);
			world.setLogic(ExtendedXMLEntityBuilder, IEntityBuilder).strong();
			world.setLogic(DefaultOperatorManager, IOperatorManager).strong();
			world.setLogic(GameActionManager);
			world.setLogic(ImageTexteManager);			
			world.setLogic(moppliq);
			
			sm.setSystem(new ResourceManager()).setProcess(ProcessPhase.TICK, int.MAX_VALUE);
			sm.setSystem(new RenderSystem(this)).setProcess(ProcessPhase.FRAME);
			sm.setSystem(new ImageTexteSystem()).setProcess(ProcessPhase.FRAME);
			sm.setSystem(GameTaskSystem).setProcess();
			sm.setSystem(new GameLevelSystem()).setProcess();
			sm.setSystem(engine).setProcess(ProcessPhase.FRAME);
			
			EntityFactory.createResourcedEntity(world.getEntityManager(), "../res/xml/alias.entityBundle.xml", "alias");
			EntityFactory.createResourcedEntity(world.getEntityManager(), "../res/xml/game.entityBundle.xml", "game");
			
			var moppliqString:String = "../res/levels/moppliq.xml";
			
			moppliq.createEntitiesFromXMLFile(moppliqString);
			engine.loadMoppliq(moppliqString);
		}
	}
}