UnitCircle
==========

Build
-----

To compile on Linux please download the [FlexSDK(4.6.0)][], then near the
`build.xml` file, create a `build.properties` file that contains:

    FLEX_HOME=<path-to-flex-sdk>
    SDK_VERSION=4.6.0
    LOCALE=en_US

You can compile with `ant` with the following command:

    ant

To execute on Linux without a web browser you can use [Flash Player Wrapper][].

[FlexSDK(4.6.0)]: http://www.adobe.com/devnet/flex/flex-sdk-download.html
[Flash Player Wrapper]: https://github.com/tdcdev/fpw
