package components 
{
	import com.ktm.genome.core.data.component.Component;
	
	public class Button extends Component
	{
		public var text:String;
		public var height:uint;
		public var width:uint;
		public var action:String;
	}
}