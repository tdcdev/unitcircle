package components 
{
	import com.ktm.genome.core.data.component.Component;
	import flash.text.TextFieldType;
	
	public class Input extends Component
	{
		public var text:String;
		public var height:uint;
		public var width:uint;
		public var restrict:String;
	}
}