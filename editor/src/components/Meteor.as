package components
{
	import com.ktm.genome.core.data.component.Component;

	public class Meteor extends Component
	{
		public var time:uint;
		public var remaind:uint;
		public var display:String;
		public var rad:Number;	
		public var radius:Number;
		public var speed:Number;
		public var life:uint;
	}
}