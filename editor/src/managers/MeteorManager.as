package managers 
{
	import com.ktm.genome.core.entity.IEntityManager;
	import com.ktm.genome.core.entity.IEntity;
	import com.ktm.genome.core.data.gene.GeneManager;
	import com.ktm.genome.core.entity.family.Family;
	import com.ktm.genome.core.data.component.IComponentMapper;
	import com.ktm.genome.core.logic.impl.LogicScope;
	import com.ktm.genome.core.entity.family.matcher.allOfGenes;
	import com.ktm.genome.core.entity.family.matcher.allOfFlags;
	import com.ktm.genome.render.component.Transform;
	import com.ktm.genome.resource.component.TextureResource;
	import com.ktm.genome.render.component.DisplayInstance;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	import flash.text.TextField;
	import components.Meteor;
	import components.Input;
	
	public class MeteorManager extends LogicScope
	{
		private static var meteorWidth:uint = 100;
		private static var meteorHeight:uint = 33;
		
		[Inject] public var em:IEntityManager;
		[Inject] public var gm:GeneManager;
		private var meteorEntities:Family;
		private var textureREntities:Family;
		private var inputEntities:Family;
		private var meteorMapper:IComponentMapper;
		private var textureRMapper:IComponentMapper;
		private var transformMapper:IComponentMapper;
		private var displayIMapper:IComponentMapper;
		private var inputMapper:IComponentMapper;
		private var meteorList:Array;
		private var entityBySprite:Dictionary;
		
		override protected function onConstructed():void
		{	
			super.onConstructed();
			
			meteorEntities = em.getFamily(allOfGenes(Meteor));
			textureREntities = em.getFamily(allOfGenes(TextureResource), allOfFlags(1));
			inputEntities = em.getFamily(allOfGenes(Input));
			meteorEntities.entityAdded.add(onMeteorAdded);
			meteorEntities.entityRemoved.add(onMeteorRemoved);
			meteorMapper = gm.getComponentMapper(Meteor);
			textureRMapper = gm.getComponentMapper(TextureResource);
			transformMapper = gm.getComponentMapper(Transform);
			displayIMapper = gm.getComponentMapper(DisplayInstance);
			inputMapper = gm.getComponentMapper(Input);
			meteorList = new Array();
			entityBySprite = new Dictionary();
		}
		
		private function onMeteorAdded(e:IEntity):void
		{
			if (textureREntities.member)
			{
				var meteor:Meteor = meteorMapper.getComponent(e);
				var displayI:DisplayInstance = displayIMapper.getComponent(e);
				var textureR:TextureResource = textureRMapper.getComponent(textureREntities.member);
				var sourceBMD:BitmapData = textureR.bitmapData;
				var destBMD:BitmapData = new BitmapData((meteor.remaind + 1) * meteorWidth, meteorHeight);
				var sourceRect:Rectangle = new Rectangle(0, 0, meteorWidth, meteorHeight);
				var destPoint:Point = new Point(0, 0);
				
				destBMD.copyPixels(sourceBMD, sourceRect, destPoint);
				sourceRect.x += meteorWidth;
				for (var i:uint = 1; i < meteor.remaind + 1; i++) 
				{
					if(i == meteor.remaind)
						sourceRect.x += meteorWidth;
					destPoint.x += meteorWidth;
					destBMD.copyPixels(sourceBMD, sourceRect, destPoint);
				}
				
				var meteorBP:Bitmap = new Bitmap(destBMD);
				var sprite:Sprite = new Sprite();
				sprite.addChild(meteorBP);
				sprite.addEventListener(MouseEvent.CLICK, clickHandler);
				entityBySprite[sprite] = e;
				
				if (displayI) 
					displayI.instance = sprite;
				else 
					em.addComponent(e, DisplayInstance, { instance: sprite } );
			
				meteorList.push(e);
				meteorList.sort(this.sortMeteorEntity);			
				this.updateDisplayedMeteor();
			}
		}
		
		private function onMeteorRemoved(e:IEntity):void
		{
			delete(entityBySprite[e]);
			meteorList.splice(meteorList.indexOf(e), 1);
			em.killEntity(e);	
		}
		
		private function clickHandler(event:MouseEvent):void
		{
			var e:IEntity = entityBySprite[event.target];
			
			if (event.ctrlKey)
			{
				this.onMeteorRemoved(e);
				this.updateDisplayedMeteor();
			}
			else
			{
				var input:Input;
				var di:DisplayInstance;
				var meteor:Meteor = meteorMapper.getComponent(e);
				
				for each (var iE:IEntity in inputEntities.members)
				{
					input = inputMapper.getComponent(iE);
					di = displayIMapper.getComponent(iE);
					
					switch(input.text)
					{
						case "time":
							TextField(di.instance).text = meteor.time.toString();
							break;
						case "remaind":
							TextField(di.instance).text = meteor.remaind.toString();
							break;
						case "display":
							TextField(di.instance).text = meteor.display;
							break;
						case "rad":
							TextField(di.instance).text = meteor.rad.toString();
							break;
						case "radius":
							TextField(di.instance).text = meteor.radius.toString();
							break;
						case "speed":
							TextField(di.instance).text = meteor.speed.toString();
							break;
						case "life":
							TextField(di.instance).text = meteor.life.toString();
							break;
						default:
							break;
					}
				}
			}
		}
		
		private function updateDisplayedMeteor():void
		{	
			var i:uint, x:uint, y:uint, xMax:uint, yMax:uint;
			var ratio:Number;
			var tab:Array = new Array(meteorList.length); // borne sup
			var m:Meteor;
			var t:Transform;
			
			if (meteorList.length > 0) // affichage a partir du min sec ou de 0sec ??
			{			
				for (y = 0; y < tab.length; y++)
				{
					tab[y] = 0;
				}
			
				for (i = 0; i < meteorList.length; i++)
				{
					m = meteorMapper.getComponent(meteorList[i]);
					t = transformMapper.getComponent(meteorList[i]);
					
					for (y = 0; y < tab.length; y++) 
					{
						if (tab[y] <= m.time) 
						{
							tab[y] = m.time + m.remaind + 1;
							
							if (y > yMax)
								yMax = y;
							if (tab[y] > xMax)
								xMax = tab[y];
								
							t.x = m.time;
							t.y = y;
							break;
						}
					}
				}
				
				var sizeX:Number = Math.min(meteorWidth, 8 * meteorWidth / xMax);
				var sizeY:Number = Math.min(meteorHeight, 10 * meteorHeight / (yMax + 1));
				var scale:Number = Math.min(sizeX / meteorWidth, sizeY / meteorHeight);
				for (i = 0; i < meteorList.length; i++)
				{
					t = transformMapper.getComponent(meteorList[i]);
					t.x *= sizeX;
					t.y *= sizeY;
					t.scaleX = scale;
					t.scaleY = scale;
				}
			}
		}
		
		private function sortMeteorEntity(e1:IEntity, e2:IEntity):int
		{
			var t1:uint = meteorMapper.getComponent(e1).time;
			var t2:uint = meteorMapper.getComponent(e2).time;
			
			if (t1 < t2) 
			{ 
				return -1; 
			} 
			else if (t1 > t2) 
			{ 
				return 1; 
			} 
			else 
			{ 
				return 0; 
			} 
		}
	}
}