package managers 
{
	import com.ktm.genome.core.entity.IEntityManager;
	import com.ktm.genome.core.entity.IEntity;
	import com.ktm.genome.core.data.gene.GeneManager;
	import com.ktm.genome.core.entity.family.Family;
	import com.ktm.genome.core.data.component.IComponentMapper;
	import com.ktm.genome.core.logic.impl.LogicScope;
	import com.ktm.genome.render.component.DisplayInstance;
	import com.ktm.genome.core.entity.family.matcher.allOfGenes;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.TextFieldType;
	import flash.events.FocusEvent;
	import flash.utils.Dictionary;
	import components.Input;
	
	public class InputManager extends LogicScope
	{
		[Inject] public var em:IEntityManager;
		[Inject] public var gm:GeneManager;
		private var inputEntities:Family;
		private var inputMapper:IComponentMapper;
		private var displayInstanceMapper:IComponentMapper;
		private var defaultTextByInput:Dictionary;
	
		override protected function onConstructed():void
		{
			super.onConstructed();
		
			inputEntities = em.getFamily(allOfGenes(Input));
			inputEntities.entityAdded.add(onInputAdded);
			inputMapper = gm.getComponentMapper(Input);
			displayInstanceMapper = gm.getComponentMapper(DisplayInstance);
			defaultTextByInput = new Dictionary();
		}
		
		private function onInputAdded(e:IEntity):void 
		{
			var input:Input = inputMapper.getComponent(e);
			var inputTxt:TextField = new TextField();
			var inputF:TextFormat = new TextFormat();
			var di:DisplayInstance = displayInstanceMapper.getComponent(e);
			
			inputTxt.text = input.text;
			inputTxt.type = TextFieldType.INPUT; 
			inputTxt.border = true;
			inputTxt.restrict = input.restrict;
			inputTxt.addEventListener(FocusEvent.FOCUS_IN, focusInHandler);
			inputTxt.addEventListener(FocusEvent.FOCUS_OUT, focusOutHandler);
			inputTxt.width = (input.width <= inputTxt.textWidth) ? inputTxt.textWidth + 4 : input.width;
			inputTxt.height = (input.height <= inputTxt.textHeight) ? inputTxt.textHeight + 4 : input.height;
			inputF.align = TextFormatAlign.LEFT;
			inputTxt.setTextFormat(inputF);
		
			defaultTextByInput[inputTxt] = input.text;
			
			if (di)
				di.instance = inputTxt;
			else 
				em.addComponent(e, DisplayInstance, { instance: inputTxt } );
		}
		
		private function focusInHandler(event:FocusEvent):void
		{
			if (event.target.text == defaultTextByInput[event.target])
				event.target.text = "";
		}
		
		private function focusOutHandler(event:FocusEvent):void
		{
			if (!event.target.text)
				event.target.text = defaultTextByInput[event.target];
		}
	}
}