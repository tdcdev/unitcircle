package managers 
{
	import com.ktm.genome.core.entity.IEntityManager;
	import com.ktm.genome.core.entity.IEntity;
	import com.ktm.genome.core.data.gene.GeneManager;
	import com.ktm.genome.core.entity.family.Family;
	import com.ktm.genome.core.data.component.IComponentMapper;
	import com.ktm.genome.core.logic.impl.LogicScope;
	import com.ktm.genome.render.component.DisplayInstance;
	import com.ktm.genome.core.entity.family.matcher.allOfGenes;
	import com.ktm.genome.core.data.gene.Gene;
	import flash.events.Event;
	import flash.net.FileReference;
	import flash.net.FileFilter;
	import flash.utils.Dictionary;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.display.Sprite;
	import flash.display.SimpleButton;
	import flash.events.MouseEvent;
	import unitcircle_editor.EntityFactory;
	import components.Button;
	import components.Meteor;
	import components.Input;
	
	public class ButtonManager extends LogicScope
	{
		[Inject] public var em:IEntityManager;
		[Inject] public var gm:GeneManager;
		private var buttonEntities:Family;
		private var meteorEntities:Family;
		private var buttonMapper:IComponentMapper;
		private var meteorMapper:IComponentMapper;
		private var displayInstanceMapper:IComponentMapper;
		private var levelLoader:FileReference;
	
		override protected function onConstructed():void
		{
			super.onConstructed();
			
			buttonEntities = em.getFamily(allOfGenes(Button));
			meteorEntities = em.getFamily(allOfGenes(Meteor));
			buttonEntities.entityAdded.add(onButtonAdded);
			buttonMapper = gm.getComponentMapper(Button);
			meteorMapper = gm.getComponentMapper(Meteor);
			displayInstanceMapper = gm.getComponentMapper(DisplayInstance);
			
			levelLoader = new FileReference();
			levelLoader.addEventListener(Event.SELECT, selectHandler);
			levelLoader.addEventListener(Event.COMPLETE, completeHandler);
		}
		
		private function onButtonAdded(e:IEntity):void 
		{
			var bt:Button = buttonMapper.getComponent(e);
			var btnTxt:TextField = new TextField();
			var btnF:TextFormat = new TextFormat();
			var down:Sprite = new Sprite();
			var btn:SimpleButton = new SimpleButton();
			var di:DisplayInstance = displayInstanceMapper.getComponent(e);
			
			btnTxt.text = bt.text;
			btnTxt.width = (bt.width <= btnTxt.textWidth) ? btnTxt.textWidth + 4 : bt.width;
			btnTxt.height = (bt.height <= btnTxt.textHeight) ? btnTxt.textHeight + 4 : bt.height;
			btnF.align = TextFormatAlign.CENTER;
			btnTxt.setTextFormat(btnF);
			
			down.graphics.beginFill(0xCCCCCC);
			down.graphics.drawRect(0, 0, btnTxt.width, btnTxt.height);	
			down.addChild(btnTxt);
			
			if (bt.action == "add" || bt.action == "save" || bt.action == "open")
				btn.addEventListener(MouseEvent.CLICK, this[bt.action]);
			btn.upState = down; btn.overState = down;
			btn.downState = down; btn.hitTestState = down;
			btn.width = down.width; 
			btn.height = down.height;
			btn.opaqueBackground = true;
				
			if (di) 
				di.instance = btn;
			else 
				em.addComponent(e, DisplayInstance, { instance: btn });
		}
		
		private function add(event:MouseEvent):void
		{	
			var inputEntities:Family = em.getFamily(allOfGenes(Input));			
			var inputMapper:IComponentMapper = gm.getComponentMapper(Input);
			var diMapper:IComponentMapper = gm.getComponentMapper(DisplayInstance);
			var e:IEntity;
			var input:Input;
			var di:DisplayInstance;
			var i:uint;
			var attributs:Dictionary = new Dictionary();
			
			for (i = 0; i < inputEntities.members.length; i++)
			{
				e = inputEntities.members[i];
				input = inputMapper.getComponent(e);
				di = diMapper.getComponent(e);
				
				if (input.text == "time" || input.text == "remaind" || input.text == "life")
					attributs[input.text] = parseInt(TextField(di.instance).text);
				else if (input.text == "rad" || input.text == "radius" || input.text == "speed")
					attributs[input.text] = parseFloat(TextField(di.instance).text);
				else
					attributs[input.text] = TextField(di.instance).text;
			}
			
			if (EntityFactory.createMeteor(em, attributs["time"], 
											   attributs["remaind"],
											   attributs["display"],
											   attributs["rad"],
											   attributs["radius"],
											   attributs["speed"],
											   attributs["life"]))
			{
				for (i = 0; i < inputEntities.members.length; i++)
				{
					e = inputEntities.members[i];
					input = inputMapper.getComponent(e);
					di = diMapper.getComponent(e);
					
					TextField(di.instance).text = input.text; // RESET INPUT FIELD
				}
			}
			else
			{
				trace("Error !");
			}
		}
		
		private function save(event:MouseEvent):void
		{
			var inputEntities:Family = em.getFamily(allOfGenes(Input));			
			var inputMapper:IComponentMapper = gm.getComponentMapper(Input);
			var diMapper:IComponentMapper = gm.getComponentMapper(DisplayInstance);
			var fileR:FileReference = new FileReference();
			var fileName:String = "level0";
			var e:IEntity;
			
			for (var i:uint = 0; i < inputEntities.members.length; i++)
			{
				e = inputEntities.members[i];
				var input:Input = inputMapper.getComponent(e);
				
				if (input.text == "backgroundImage") 
				{
					var di:DisplayInstance = diMapper.getComponent(e);	
					fileName = TextField(di.instance).text;
					break;
				}
			}
			
			var content:String = '<?xml version="1.0" encoding="utf-8"?>\n\n';
			content += '<Resource>\n\n';
			content += '\t<e>\n';
			content += '\t\t<Layered layerId="backgroundLayer" />\n';
			content += '\t\t<TextureResource source="../res/img/' + 
								'background_' + fileName + 
								'.png" id="background_' + fileName + '" />\n';
			content += '\t\t<Transform />\n';
			content += '\t\t<Atom rad="0" dist="0" radius="1.0" visible="true" />\n';
        	content += '\t</e>\n\n';
			
			for each (e in meteorEntities.members)
			{
				var m:Meteor = meteorMapper.getComponent(e);
				
				content += '\t<e>\n';
				content += '\t\t<Layered layerId="entryLayer" />\n';
				content += '\t\t<ImageTexte source="../res/img/back.png"' + 
										   ' text=" " border="5"' + 
										   ' color="0xFF0000" width="200" />\n';
				content += '\t\t<Transform x="0" y="0" />\n';
				content += '\t\t<Entry time="' + m.time + 
									'" remaind="' + m.remaind +
									'" display="' + m.display + 
									'" rad="' + m.rad +
									'" radius="' + m.radius + 
									'" speed="' + m.speed + 
									'" life="' + m.life + '" />\n';
				content += '\t</e>\n\n';
			}
			
			content += '</Resource>\n';
			fileR.save(content, fileName + '.entityBundle.xml');			
		}
		
		private function open(event:MouseEvent):void
		{
			levelLoader.browse([new FileFilter(".xml", "*.xml")]);
		}
		
		private function selectHandler(event:Event):void
		{
			levelLoader.load();
		}
		
		private function completeHandler(event:Event):void
		{
			var meteorG:Gene = GeneManager.getGeneFor(Meteor);
			var level:XML = new XML(levelLoader.data);
			
			for (var i:uint = 0; i < meteorEntities.members.length; i++)
			{
				em.removeComponent(meteorEntities.members[i], meteorG);
			}
			
			for each (var node:XML in level.e.Entry)
			{
				EntityFactory.createMeteor(em, node.@time, node.@remaind,
											   node.@display, node.@rad,
											   node.@radius, node.@speed, 
											   node.@life);
			}
		}
	}
}
