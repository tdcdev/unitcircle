package unitcircle_editor 
{
	import com.ktm.genome.core.entity.IEntity;
	import com.ktm.genome.core.entity.IEntityManager;
	import com.ktm.genome.resource.component.EntityBundle;
	import com.ktm.genome.render.component.Transform;
	import com.ktm.genome.render.component.Layered;
	import com.ktm.genome.render.component.DisplayInstance;
	import components.Meteor;
	
	public class EntityFactory 
	{
		static public function createResourcedEntity(em:IEntityManager, source:String, id:String):IEntity 
		{
			var e:IEntity = em.create();
			em.addComponent(e, EntityBundle, {source: source, id: id, toBuild: true});
			return e;
		}
		
		static public function createMeteor(em:IEntityManager, time:Number,
															   remaind:Number,
															   display:String,
															   rad:Number,
															   radius:Number,
															   speed:Number,
															   life:Number):IEntity
		{		
			if (isNaN(time) || time < 0 ||
				isNaN(remaind) || remaind < 1 || remaind > 60 || // Taille du bitmap
				!display || display == "display" ||
				isNaN(rad) ||
				isNaN(radius) ||
				isNaN(speed) ||
				isNaN(life) || life < 1)
				return null;
			
			var e:IEntity = em.create();
			em.addComponent(e, Meteor, { time: time, remaind: remaind,
										 display: display, rad: rad,
										 radius: radius, speed: speed,
										 life: life } );
			em.addComponent(e, Transform, { visible: true } );
			em.addComponent(e, Layered, { layerId: "meteorLayer" } );
			em.addComponent(e, DisplayInstance);
			return e;
		}
	}
}