package unitcircle_editor
{
	import com.ktm.genome.core.BigBang;
	import com.ktm.genome.core.IWorld;
	import com.ktm.genome.core.logic.system.ISystemManager;
	import com.ktm.genome.core.logic.process.ProcessPhase;
	import com.ktm.genome.render.system.RenderSystem;
	import com.ktm.genome.resource.manager.ResourceManager;
	import flash.display.Sprite;
	import managers.ButtonManager;
	import managers.MeteorManager;
	import managers.InputManager;
	
	[SWF(width="915",height="368")]
	
	public class Main extends Sprite 
	{
		public function Main():void 
		{
			var world:IWorld = BigBang.createWorld(stage);	
			var sm:ISystemManager = world.getSystemManager();

			world.setLogic(ButtonManager);
			world.setLogic(MeteorManager);
			world.setLogic(InputManager);
			
			sm.setSystem(new ResourceManager()).setProcess(ProcessPhase.TICK, int.MAX_VALUE);
			sm.setSystem(new RenderSystem(this)).setProcess(ProcessPhase.FRAME);			
			
			EntityFactory.createResourcedEntity(world.getEntityManager(), "../res/xml/alias.entityBundle.xml", "alias");	
			EntityFactory.createResourcedEntity(world.getEntityManager(), "../res/xml/game.entityBundle.xml", "game");	
		}
	}
}